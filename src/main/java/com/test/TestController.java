package com.test;

import com.util.AuthorSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;

@RestController
public class TestController{
    private final static Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private AuthorSettings authorSettings;

    @RequestMapping(value = "/hello")
    public String hello(){
        String name = authorSettings.getName()+ "年龄：" + authorSettings.getAge();
        logger.info("1111111");
        return name;
    }
}
